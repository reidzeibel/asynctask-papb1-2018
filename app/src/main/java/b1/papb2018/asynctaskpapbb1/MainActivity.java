package b1.papb2018.asynctaskpapbb1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView mTitleText, mAuthorText;
    EditText mBookInput;
    String mQueryString; // tidak harus ada

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi
        mBookInput = findViewById(R.id.bookInput);
        mTitleText = findViewById(R.id.titleText);
        mAuthorText = findViewById(R.id.authorText);
    }

    public void searchBooks(View view){
        mQueryString = mBookInput.getText().toString();
        GoogleBooks googleBooks = new GoogleBooks();
        new FetchBook(mTitleText, mAuthorText).execute(mQueryString);
    }
}
