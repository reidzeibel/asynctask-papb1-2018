package b1.papb2018.asynctaskpapbb1;

import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FetchBook extends AsyncTask<String,Void,String> {

    private TextView mTitleText;
    private TextView mAuthorText;

    public FetchBook(TextView mTitleText, TextView mAuthorText) {
        this.mTitleText = mTitleText;
        this.mAuthorText = mAuthorText;
    }

    @Override
    protected String doInBackground(String... params) {
        return NetworkUtils.getBookInfo(params[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            // {...} Objek Utama
            JSONObject jsonObject = new JSONObject(s);

            // ambil bagian "items"
            JSONArray itemsArray = jsonObject.getJSONArray("items");

            // siapkan Objek GoogleBooks
            GoogleBooks googleBooks = new GoogleBooks();

            // inisialisasi objek Googlebooks, dengan nilai dari JSON
            googleBooks.kind = jsonObject.getString("kind");
            googleBooks.totalItems = jsonObject.getInt("totalItems");

            // untuk items, inisialisasi dengan sebuah list kosong, karena data buku belum diparsing
            googleBooks.items = new ArrayList<>(itemsArray.length());

            // iterasi data buku (items)
            for(int i = 0; i<itemsArray.length(); i++){

                // inisialisasi objek Buku
                Buku buku = new Buku();

                // ambil objek JSON untuk Buku
                JSONObject book = itemsArray.getJSONObject(i);
                // ambil objek JSON untuk VolumeInfo
                JSONObject volumeInfoJson = book.getJSONObject("volumeInfo");
                // ambil array JSON authors
                JSONArray authors = volumeInfoJson.getJSONArray("authors");

                // Buat objek VolumeInfo
                VolumeInfo volumeInfo = new VolumeInfo();

                // masukkan title dan authors kedalam VolumeInfo
                volumeInfo.title = volumeInfoJson.getString("title");
                volumeInfo.authors = new ArrayList<>();
                for (int x = 0; x<authors.length(); x++) {
                    volumeInfo.authors.add(authors.getString(x));
                }

                // masukkan objek VolumeInfo kedalam objek Buku
                buku.volumeInfo = volumeInfo;

                // Tambahkan objek buku kedalam array items di objek googleBooks
                googleBooks.items.add(buku);
            }

            mTitleText.setText(googleBooks.items.get(0).volumeInfo.title);
            mAuthorText.setText(googleBooks.items.get(0).volumeInfo.authors.get(0));

        } catch (Exception e) {
            mTitleText.setText("No Results Found");
            mAuthorText.setText("");
            e.printStackTrace();
        }

    }
}
